import React from 'react'

import stepVerificationStyles from '../styles/stepVerification.module.scss'


const StepTwo = ({onAction, ...props}) => {
  return(
    <div className={`${stepVerificationStyles.stepItem} ${stepVerificationStyles.stepTwo} ${(props.active) ? stepVerificationStyles.stepItemActive : ''} ${stepVerificationStyles.stepItem} ${(props.completed) ? stepVerificationStyles.stepItemCompleted : ''} ${stepVerificationStyles.stepItem} ${(props.disabled) ? stepVerificationStyles.stepItemDisabled : ''}`}>
          <p className={stepVerificationStyles.stepTitle}>
            Select a method
          </p>
         <form onSubmit={onAction}>
           <div className={stepVerificationStyles.labelContainer}>
            <label htmlFor="sms"><input className={stepVerificationStyles.radioButtonInput} type="radio" id="sms" value="sms" name="method" data-contact="*** *** 3456"/>
              <span>Text message (SMS)</span>
              <span>*** *** 3456</span>
            </label>
           </div>
           <div className={stepVerificationStyles.labelContainer}>
            <label htmlFor="call"><input className={stepVerificationStyles.radioButtonInput} type="radio" id="call" value="call" name="method" data-contact="*** *** 3456"/>
            <span>Voice call</span>
            <span>*** *** 3456</span>
            </label>
           </div>
           <div className={stepVerificationStyles.labelContainer}>
            <label htmlFor="mail"><input className={stepVerificationStyles.radioButtonInput} type="radio" id="mail" value="mail" name="method" data-contact="f****@iqvia.com"/>
              <span>Email</span>
              <span>f****@iqvia.com</span>
            </label>
           </div>
           <button type="submit" value="Send code" className={`${stepVerificationStyles.primaryButton} ${stepVerificationStyles.buttonSmall}`}>Send code</button>
         </form>
        </div>
  )
}

export default StepTwo