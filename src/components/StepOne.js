import React from 'react'

import stepVerificationStyles from '../styles/stepVerification.module.scss'


const StepOne = ({onAction, ...props}) => {
  return(
    <div className={`${stepVerificationStyles.stepItem} ${stepVerificationStyles.stepOne} ${(props.active) ? stepVerificationStyles.stepItemActive : ''} ${stepVerificationStyles.stepItem} ${(props.completed) ? stepVerificationStyles.stepItemCompleted : ''} ${stepVerificationStyles.stepItem} ${(props.disabled) ? stepVerificationStyles.stepItemDisabled : ''}`}>
    <p className={stepVerificationStyles.stepTitle}>
      Update Response
    </p>
     <div className={stepVerificationStyles.oneCheckbox}>
        <label htmlFor="acceptOption1" onClick={() => onAction(true)} className={stepVerificationStyles.radioButtonText}><input className={stepVerificationStyles.radioButtonInput} type="radio"  name="radio" id="acceptOption1"/>
        <span>I give my consent</span></label>
      </div>
      <div className={stepVerificationStyles.oneCheckbox}>
        <label htmlFor="acceptOption2" onClick={() => onAction(false)} className={stepVerificationStyles.radioButtonText}><input className={stepVerificationStyles.radioButtonInput} type="radio" name="radio" id="acceptOption2"/><span>I do not give my consent</span></label>
      </div>
    </div>
  )
}

export default StepOne