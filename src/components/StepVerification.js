import React, {useState} from 'react'

import StepOne from './StepOne'
import StepTwo from './StepTwo'
import stepVerificationStyles from '../styles/stepVerification.module.scss'

const StepVerification = () =>{
  
//Item 1//

const [completeItem1, setCompleteItem1] = useState(false)
const [activeItem1, setActiveItem1] = useState(true)
const [disabledItem1, setDisabledItem1] = useState(false)

//Item 2 //

const [completeItem2, setCompleteItem2] = useState(false)
const [activeItem2, setActiveItem2] = useState(false)
const [disabledItem2, setDisabledItem2] = useState(true)

//Item 3 //
const [completeItem3, setCompleteItem3] = useState(false)
const [activeItem3, setActiveItem3] = useState(false)
const [disabledItem3, setDisabledItem3] = useState(true)

const [message,setMessage] = useState(false)
const [codeMessage,setCodeMessage] = useState('')


const handleSubmit = (e) => {
  e.preventDefault();
    setCompleteItem2(true)
    setActiveItem2(false)
    setDisabledItem2(false)
    setActiveItem3(true)
    setDisabledItem3(false)
    
    let elem = document.getElementById(e.target.method.value);
    let contact = elem.getAttribute('data-contact');
    let method= e.target.method.value

    switch(method) {
      case "sms":
        setCodeMessage(`An SMS has been sent to your mobile phone.\nPlease check your messages (${contact}) to obtain the validation code`)
        break;
      case "call":
        setCodeMessage(`A Voice call will be made to your mobile phone.\nPlease check your messages (${contact}) to obtain the validation code`)
        break;
      case "mail":
        setCodeMessage(`An Email has been sent to your email adress.\nPlease check your messages (${contact}) to obtain the validation code`)
        break;
      default:
        //Code
    }
    sendCode()
}

const updateResponse = (value) => {
  if(value){
    setCompleteItem1(true)
    setActiveItem1(false) 
    setDisabledItem1(false)
    setActiveItem2(true)
    setDisabledItem2(false)
  }else{
    setCompleteItem1(false)
    setActiveItem1(true) 
    setDisabledItem1(false)
    setActiveItem2(false)
    setDisabledItem2(true)
    setCompleteItem2(false)
    setActiveItem3(false)
    setDisabledItem3(true)
    setCompleteItem3(false)
  }
}

const onlyNumbers = (event) =>{

  if(!(/\d/.test(event.key))){
    setMessage(true)
  }  
  }

const checkValue = (e) =>{
 
 if(e.target.value === '' || (/^\d+$/.test(e.target.value))){
   setMessage(false)
 }
}

function sendCode() {
  var chars = "1234567890",
  pass  = "",
  PL    = 12;
  
  for (var x = 0; x < PL; x++) {
  var i = Math.floor(Math.random() * chars.length);
  pass += chars.charAt(i);
  }
  
  localStorage.setItem('code', pass);
  
  return alert(`The code is ${pass}`);
  }

  
  const verifyCode = () =>{
    let newCode = localStorage.getItem('code')
    let storageCode = document.getElementById('codeInput').value
  
    if(storageCode === newCode){
      alert('Your code has been verify')
    }else{
      alert('Your code is not correct')
    }
  }

  return(
    <div className={stepVerificationStyles.stepContainer}>
      <div className={stepVerificationStyles.stepList}>
        <StepOne onAction={updateResponse} completed={completeItem1} active={activeItem1} disabled={disabledItem1} />
        <StepTwo onAction={handleSubmit} completed={completeItem2} active={activeItem2} disabled={disabledItem2} />
    
       <div className={`${stepVerificationStyles.stepItem} ${stepVerificationStyles.stepThree} ${(activeItem3) ? stepVerificationStyles.stepItemActive : ''} ${stepVerificationStyles.stepItem} ${(completeItem3) ? stepVerificationStyles.stepItemCompleted : ''} ${stepVerificationStyles.stepItem} ${(disabledItem3) ? stepVerificationStyles.stepItemDisabled : ''}`}>
          <p className={stepVerificationStyles.stepTitle}>
            Enter Code
          </p>
          <p className={stepVerificationStyles.sentMessage}>{codeMessage}</p>
          <form name="codeform" method="post" action="">
            <input className={`${(message) ? stepVerificationStyles.alert : ''}`} id="codeInput" placeholder="Code" type="text" onKeyPress={(e) => onlyNumbers(e)} onChange={(e) => checkValue(e)}/>
            {
              (message ? <p className={stepVerificationStyles.alertMessage}>Only numbers are accepted</p> : '')
            }
            <button className={stepVerificationStyles.nudeButton} type="button" value="Generate" name="code" onClick={(pass) => sendCode(pass)}>Resend Code</button>
            <button className= {`${stepVerificationStyles.primaryButton} ${stepVerificationStyles.buttonLarge}`} type="button" onClick={() => verifyCode()}>
              Verify
            </button>
          </form>
        </div>
      </div>
     </div>
  
  )
}

export default StepVerification