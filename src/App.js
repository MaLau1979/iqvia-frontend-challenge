import StepVerification from './components/StepVerification'
import './styles/app.scss'

function App() {
  return (
   <div className="container">
    <StepVerification />
   </div>
  )
}

export default App;
